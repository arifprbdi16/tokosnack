<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function regis(){
        return view ('regis');
    }

    public function proses_register(){
        return view ('proses_register');
    }

    public function login_admin(){
        return view ('login_admin');
    }

    public function proses_login(){
        return view ('proses_login');
    }

    public function login_pemasok(){
        return view ('login_pemasok');
    }
}
