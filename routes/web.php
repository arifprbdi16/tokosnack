<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'HomeController@home');
// Route::get('/register', 'AuthController@register');
// Route::post('/index', 'AuthController@form');

route::get('/regis', 'RegisterController@regis');
route::get('/login_admin', 'RegisterController@login_admin');
route::get('/login_admin', 'RegisterController@login_pemasok');
route::post('/proses_login', 'RegisterController@proses_login');
route::post('/proses_register', 'RegisterController@proses_register');

route::post('/index', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
